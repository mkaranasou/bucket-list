"use strict";
var ActionType;
(function (ActionType) {
    ActionType[ActionType["none"] = 0] = "none";
    ActionType[ActionType["itemComplete"] = 1] = "itemComplete";
    ActionType[ActionType["itemAdded"] = 2] = "itemAdded";
    ActionType[ActionType["itemRemoved"] = 3] = "itemRemoved";
    ActionType[ActionType["itemFavorited"] = 4] = "itemFavorited";
    ActionType[ActionType["bucketAdded"] = 5] = "bucketAdded";
    ActionType[ActionType["bucketRemoved"] = 6] = "bucketRemoved";
    ActionType[ActionType["bucketComplete"] = 7] = "bucketComplete";
    ActionType[ActionType["bucketFavorited"] = 8] = "bucketFavorited";
})(ActionType = exports.ActionType || (exports.ActionType = {}));
var Action = (function () {
    function Action(options) {
        options = options || {};
        this.id = options.id || null;
        this.type = options.type || ActionType.none;
        this.actor = options.actor || null;
        this.item = options.item || null;
        this.message = this.getMessage();
    }
    Action.prototype.getMessage = function () {
        switch (this.type) {
            case ActionType.none:
                return "";
            case ActionType.itemComplete:
                return "User " + this.actor.name + " completed '" + this.item.description + "'!!!";
            case ActionType.itemAdded:
                return "User " + this.actor.name + " added '" + this.item.description + "'!";
            case ActionType.itemRemoved:
                return "User " + this.actor.name + " removed '" + this.item.description + "'.";
            case ActionType.itemFavorited:
                return "User " + this.actor.name + " liked '" + this.item.description + "'!";
            case ActionType.bucketAdded:
                return "User " + this.actor.name + " added '" + this.item.description + "' bucket!";
            case ActionType.bucketRemoved:
                return "User " + this.actor.name + " removed '" + this.item.description + "' bucket.";
            case ActionType.bucketComplete:
                return "User " + this.actor.name + " completed '" + this.item.description + "' bucket!!!";
            case ActionType.bucketFavorited:
                return "User " + this.actor.name + " liked '" + this.item.description + "' bucket!";
            default:
                return "";
        }
    };
    return Action;
}());
exports.Action = Action;
var Item = (function () {
    function Item(options) {
        options = options || {};
        this.id = options.id || null;
        this.description = options.description || "No description";
        this.bucket_id = options.bucket_id || null;
    }
    return Item;
}());
exports.Item = Item;
var Bucket = (function () {
    function Bucket(options) {
        options = options || {};
        this.id = options.id || null;
        this.name = options.name || "no name";
        this.description = options.description || "No description";
        this.items = options.items || [];
    }
    return Bucket;
}());
exports.Bucket = Bucket;
var User = (function () {
    function User(options) {
        options = options || {};
        this.id = options.id || null;
        this.name = options.name || null;
        this.profile = options.profile || {};
        this.is_valid = options.is_valid === true;
        this.update_others = options.update_others === true;
        this.buckets = options.buckets || [];
    }
    return User;
}());
exports.User = User;
var Dashboard = (function () {
    function Dashboard() {
    }
    return Dashboard;
}());
exports.Dashboard = Dashboard;
