import { Component } from '@angular/core';
import {Dashboard, User, Action, ActionType, Bucket} from './models';

@Component({
  selector: 'my-buckets',
  templateUrl: "./templates/my-buckets.component.html",
  //styles: ["/css/"]
})
export class MyBucketsComponent {
  user:User;
  actions: Action[];

  name = 'Angular';

  constructor(){
    this.user = new User({name:"Maria"});
    this.actions = [];
    this.user.buckets = [];

    for(let i=0; i<10; i++){

      let a = new Action({id:i, type:ActionType.bucketAdded, actor:this.user, item:{description:"Some Bucket"}});
      this.actions.push(a);
      this.user.buckets.push(new Bucket({description:"Some Bucket"}))
    }

  }
}
