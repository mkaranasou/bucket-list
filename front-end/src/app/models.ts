export enum ActionType{
  none,
  itemComplete,
  itemAdded,
  itemRemoved,
  itemFavorited,
  bucketAdded,
  bucketRemoved,
  bucketComplete,
  bucketFavorited
}

export class Action{
  id:string;
  type:ActionType;
  actor: User;
  item:any;
  message: string

  constructor(options?){
    options = options || {};
    this.id = options.id || null;
    this.type = options.type || ActionType.none;
    this.actor = options.actor || null;
    this.item = options.item || null;
    this.message = this.getMessage();
  }

  getMessage():string{
    switch (this.type){ // todo: make this smarter..
      case ActionType.none:
        return "";
      case ActionType.itemComplete:
        return "User " + this.actor.name + " completed '" + this.item.description + "'!!!";
      case ActionType.itemAdded:
        return "User " +  this.actor.name + " added '" + this.item.description + "'!";
      case ActionType.itemRemoved:
        return "User " + this.actor.name + " removed '" + this.item.description + "'.";
      case ActionType.itemFavorited:
        return "User " + this.actor.name + " liked '" + this.item.description + "'!";
      case ActionType.bucketAdded:
        return "User " + this.actor.name + " added '" + this.item.description + "' bucket!";
      case ActionType.bucketRemoved:
        return "User " + this.actor.name + " removed '" + this.item.description + "' bucket.";
      case ActionType.bucketComplete:
        return "User " + this.actor.name + " completed '" + this.item.description + "' bucket!!!";
      case ActionType.bucketFavorited:
        return "User " + this.actor.name + " liked '" + this.item.description + "' bucket!";
      default:
        return "";
    }
  }
}

export class Item{
  id:string;
  description:string;
  bucket_id:string;

  constructor(options?){
    options = options || {};
    this.id = options.id || null;
    this.description = options.description || "No description";
    this.bucket_id = options.bucket_id || null;
  }
}

export class Bucket{
  id:string;
  name:string;
  description:string;
  items: Item[];

  constructor(options?){
    options = options || {};
    this.id = options.id || null;
    this.name = options.name || "no name";
    this.description = options.description || "No description";
    this.items = options.items || [];
  }
}

export class User{
  id:string;
  name:string;
  profile:any;
  is_valid:boolean;
  update_others: boolean;
  buckets:Bucket[];

  constructor(options?){
    options = options || {};
    this.id = options.id || null;
    this.name = options.name || null;
    this.profile = options.profile || {};
    this.is_valid = options.is_valid === true;
    this.update_others = options.update_others === true;
    this.buckets =  options.buckets || [];
  }
}

export class Dashboard{
  user: User;
  actions: Action[];
}

export class UserProfile{
  email:string;
  email_verified: boolean;
  name:string;
  given_name: string;
  family_name: string;
  picture: string;
  gender: string;
  locale: string;
  updated_at: string;
  nickname: string;

  constructor(options?){
    options = options || {};
    this.email = options.email || null;
    this.email_verified = options.email_verified || null;
    this.name = options.name || null;
    this.given_name = options.given_name || null;
    this.family_name = options.family_name || null;
    this.picture = options.picture || null;
    this.gender = options.gender || null;
    this.locale = options.locale || null;
    this.updated_at = options.updated_at || null;
    this.nickname = options.nickname || null;
  }
}
