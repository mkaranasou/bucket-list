import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'
import { tokenNotExpired } from 'angular2-jwt';
import { myConfig } from './auth.config';
import {UserProfile} from "../models";

// Avoid name not found warnings
//declare var Auth0Lock: any;
let Auth0Lock: any = require('auth0-lock').default;

@Injectable()
export class AuthService {
  // Configure Auth0
  lock = new Auth0Lock(myConfig.clientID, myConfig.domain,

    /*{
     auth: {
     redirectUrl: myConfig.callbackUrl
     }
     }*/
  );

  constructor() {
    // Add callback for lock `authenticated` event
    this.lock.on("authenticated", (authResult) => {
      localStorage.setItem('id_token', authResult.idToken);

      this.lock.getProfile(authResult.idToken, (error:any, profile: any): void => {
        if (error) {
          throw error;
        }

        localStorage.setItem('profile', JSON.stringify(profile));
      });
      this.lock.hide();
    });
  }

  public register(){
    // todo
  }

  public getProfile(){
    return new UserProfile(JSON.parse(localStorage.getItem('profile')));
  }

  public login() {
    // Call the show method to display the widget.
    this.lock.show((error: string, profile: Object, id_token: string) => {
      if (error) {
        console.log(error);
      }
      // We get a profile object for the user from Auth0
      localStorage.setItem('profile', JSON.stringify(profile));
      // We also get the user's JWT
      localStorage.setItem('id_token', id_token);
    });
  }

  public authenticated() {
    // Check if there's an unexpired JWT
    // This searches for an item in localStorage with key == 'id_token'
    return tokenNotExpired();
  }

  public logout() {
    // To log out, we just need to remove
    // the user's profile and token
    localStorage.removeItem('profile');
    localStorage.removeItem('id_token');
  }
}
