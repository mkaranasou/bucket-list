﻿interface AuthConfiguration {
    clientID: string,
    domain: string,
    callbackUrl: string
}

export const myConfig: AuthConfiguration = {
    clientID: 'CZT3E2Vvz0w62AQ2IJnx5eZldyDBa6tp',
    domain: 'digitalsense.eu.auth0.com',
    callbackUrl: 'http://localhost:5000/callback',
};
