import { Component } from '@angular/core';
import { Dashboard, User, Action, ActionType } from './models';

@Component({
  selector: 'bucket-stream',
  templateUrl: "./templates/bucket-stream.component.html",
  //styles: ["/css/"]
})
export class BucketStreamComponent {
  dashboard:Dashboard;
  actions: Action[];

  name = 'Angular';

  constructor(){
    this.dashboard = new Dashboard();
    this.dashboard.user = new User({name:"Maria"});
    this.actions = [];

    for(let i=0; i<10; i++){

      let a = new Action({id:i, type:ActionType.bucketAdded, actor:this.dashboard.user, item:{description:"Some Bucket"}});
      this.actions.push(a);
    }

  }
}
