import { Component } from '@angular/core';
import { RouterOutlet, RouterLink, RouterLinkWithHref,
  RouterLinkActive } from '@angular/router';
import {AuthService} from "./services/auth.service";

@Component({
  selector: 'main-bucket',
  templateUrl: './templates/app.component.html',
  providers: [AuthService]
})
export class AppComponent {

  name = 'Angular';

  constructor(private auth: AuthService) {

  }

}
