import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {HttpModule, Http, RequestOptions} from '@angular/http';
import { BucketStreamComponent }  from './bucket-stream.component';
import { TopComponent }  from './top.component';
import { BottomComponent }  from './bottom.component';
import { AppComponent } from './app.component';
import {RouterModule} from "@angular/router";
import {AuthHttp, AuthConfig} from "angular2-jwt";
import {AuthGuard} from "./services/guards/auth.guard";
import {AuthService} from "./services/auth.service";
import {MyBucketsComponent} from "./my-buckets.component";
import {HomeComponent} from "./home.component";

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig(), http, options);
}

@NgModule({
  declarations: [
    AppComponent,
    BucketStreamComponent,
    TopComponent,
    BottomComponent,
    MyBucketsComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      {
        path: 'dashboard',
        component: BucketStreamComponent
      },
      {
        path: 'my-buckets',
        component: MyBucketsComponent
      },
      { path: '', component: HomeComponent },
      // { path: '', component: AppComponent },
      // otherwise redirect to home
      { path: '**', redirectTo: '' }
    ]),
  ],
  providers: [RouterModule,
    {
    provide: AuthHttp,
    useFactory: authHttpServiceFactory,
    deps: [ Http, RequestOptions ]
  },
    AuthService,
    AuthGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}
