import { Component } from '@angular/core';
import {AuthService} from "./services/auth.service";

@Component({
  selector: 'top',
  templateUrl: `./templates/top.component.html`,
})
export class TopComponent {
  name = 'Angular';

  constructor(private auth:AuthService){

  }

}

