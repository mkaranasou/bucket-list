"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var models_1 = require("./models");
var BucketStreamComponent = (function () {
    function BucketStreamComponent() {
        this.name = 'Angular';
        this.dashboard = new models_1.Dashboard();
        this.dashboard.user = new models_1.User({ name: "Maria" });
        this.actions = [];
        for (var i = 0; i < 10; i++) {
            var a = new models_1.Action({ id: i, type: models_1.ActionType.bucketAdded, actor: this.dashboard.user, item: { description: "Some Bucket" } });
            this.actions.push(a);
        }
    }
    return BucketStreamComponent;
}());
BucketStreamComponent = __decorate([
    core_1.Component({
        selector: 'main-container',
        templateUrl: "./templates/bucket-stream.component.html"
    })
], BucketStreamComponent);
exports.BucketStreamComponent = BucketStreamComponent;
