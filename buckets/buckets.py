import json
import jwt
import os
from flask import flash
from flask import render_template
from flask import session
import requests
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from werkzeug.utils import secure_filename
from functools import wraps
from flask import Flask, request, jsonify, _app_ctx_stack, redirect
from dotenv import Dotenv
from flask_cors import cross_origin


try:
    env = Dotenv('./.env')
    client_id = env["AUTH0_CLIENT_ID"]
    client_secret = env["AUTH0_CLIENT_SECRET"]
except IOError:
    client_id = "CZT3E2Vvz0w62AQ2IJnx5eZldyDBa6t"
    env = os.environ

UPLOAD_FOLDER = '/Users/mariakaranasou/Projects/bucket-list/buckets/static/uploads'
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}
app = Flask(__name__)
app.config.from_pyfile('config.py')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
CORS(app)

db = SQLAlchemy(app)

db.create_all()

FRONT_END_URL = "localhost:4200"


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

# Format error response and append status code.
def handle_error(error, status_code):
    resp = jsonify(error)
    resp.status_code = status_code
    return resp


def requires_auth_(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.headers.get('Authorization', None)
        if not auth:
            return handle_error({'code': 'authorization_header_missing',
                                'description':
                                    'Authorization header is expected'}, 401)

        parts = auth.split()

        if parts[0].lower() != 'bearer':
            return handle_error({'code': 'invalid_header',
                                'description':
                                    'Authorization header must start with'
                                    'Bearer'}, 401)
        elif len(parts) == 1:
            return handle_error({'code': 'invalid_header',
                                'description': 'Token not found'}, 401)
        elif len(parts) > 2:
            return handle_error({'code': 'invalid_header',
                                'description': 'Authorization header must be'
                                 'Bearer + \s + token'}, 401)

        token = parts[1]
        try:
            payload = jwt.decode(
                token,
                client_secret,
                audience=client_id
            )
        except jwt.ExpiredSignature:
            return handle_error({'code': 'token_expired',
                                'description': 'token is expired'}, 401)
        except jwt.InvalidAudienceError:
            return handle_error({'code': 'invalid_audience',
                                'description': 'incorrect audience, expected: '
                                 + client_id}, 401)
        except jwt.DecodeError:
            return handle_error({'code': 'token_invalid_signature',
                                'description':
                                    'token signature is invalid'}, 401)
        except Exception:
            return handle_error({'code': 'invalid_header',
                                'description': 'Unable to parse authentication'
                                 ' token.'}, 400)

        _app_ctx_stack.top.current_user = payload
        return f(*args, **kwargs)

    return decorated


def requires_auth(f):
  @wraps(f)
  def decorated(*args, **kwargs):
    if 'profile' not in session:
      # Redirect to Login page here
      return redirect('/')
    return f(*args, **kwargs)

  return decorated

# Here we're using the /callback route.
@app.route('/callback')
def callback_handling():
  code = request.args.get('code')
  json_header = {'content-type': 'application/json'}
  token_url = "https://{domain}/oauth/token".format(domain='digitalsense.eu.auth0.com')

  token_payload = {
    'client_id':     'CZT3E2Vvz0w62AQ2IJnx5eZldyDBa6tp',
    'client_secret': 'vr-AT7qNKdwlSUm6GuT2Uti8wdB2qNPx1RIfXm8H_-kfF3omjdbSiEB-h5Ogz_EO',
    'redirect_uri':  'http://localhost:5000/callback',
    'code':          code,
    'grant_type':    'authorization_code'
  }

  token_info = requests.post(token_url, data=json.dumps(token_payload), headers=json_header).json()
  user_url = "https://{domain}/userinfo?access_token={access_token}".format(domain='digitalsense.eu.auth0.com',
                                                                            access_token=token_info['access_token'])

  user_info = requests.get(user_url).json()
  # We're saving all user information into the session
  session['profile'] = user_info
  # Redirect to the User logged in page that you want here
  # In our case it's /dashboard
  return redirect(FRONT_END_URL + '/dashboard')


# http://flask.pocoo.org/docs/0.12/patterns/fileuploads/
@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return redirect(url_for('uploaded_file',
                                    filename=filename))
    return render_template("upload.html")

@app.route('/')
@cross_origin(headers=['Content-Type', 'Authorization'])
def buckets_index():
    return render_template('index.html', user=session.get('profile', {}))


@app.route('/buckets', methods=["GET", "POST"])
@cross_origin(headers=['Content-Type', 'Authorization'])
def buckets():
    return render_template('my_buckets.html', user=session['profile'])


@app.route('/buckets/my-buckets', methods=["GET", "POST", "PUT", "DELETE"])
@cross_origin(headers=['Content-Type', 'Authorization'])
def my_buckets():
    return render_template('my_buckets.html', user=session['profile'])


@app.route('/buckets/<bucket_id>', methods=["GET"])
@cross_origin(headers=['Content-Type', 'Authorization'])
def bucket(bucket_id):
    return render_template('my_buckets.html', user=session['profile'])


@app.route("/actions", methods=["GET", "POST"])
@requires_auth
def actions():
    return render_template('dashboard.html', user=session['profile'])


@app.route("/items", methods=["GET", "POST", "PUT", "DELETE"])
@requires_auth
def items():
    return render_template('dashboard.html', user=session['profile'])



@app.route("/secured/ping")
@cross_origin(headers=['Content-Type', 'Authorization'])
@cross_origin(headers=['Access-Control-Allow-Origin', '*'])
@requires_auth
def securedPing():
    return "All good. You only get this message if you're authenticated"


if __name__ == '__main__':
    app.run(debug=True)
