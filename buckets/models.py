import flask_admin
from enum import Enum
from flask_security import RoleMixin, UserMixin
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import ForeignKey
from buckets import db


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def __str__(self):
        return self.name


class AttachmentType(Enum):
    Picture = 0,
    Text = 1


class Attachment(db.Model):
    __tablename__ = "attachment"
    id = db.Column(db.Integer, primary_key=True)
    file_path = db.Column(db.String())
    file_type = db.Column(db.String())
    item_id = db.Column(db.Integer(), ForeignKey('bucket_item.id'))
    # bucket_item = db.relationship("BucketItem", back_populates="attachments")

    def __repr__(self):
        return '<BucketItem %r>' % self.description


class BucketItem(db.Model):
    """
    A bucket item MUST have a bucket parent and may have one or more hearts
    """
    __tablename__ = "bucket_item"
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(255))
    is_complete = db.Column(db.Boolean, default=False)
    completion_date = db.Column(db.DateTime())
    create_date = db.Column(db.DateTime())
    is_active = db.Column(db.Boolean, default=True)
    is_public = db.Column(db.Boolean, default=False)
    bucket_id = db.Column(db.Integer(), ForeignKey('bucket.id'))
    bucket = db.relationship("Bucket", back_populates="items")
    hearts = db.relationship('BucketItemHeart', back_populates='bucket_item_heart.item_id')
    attachments = db.relationship('Attachment', back_populates='attachment.item_id')

    def __repr__(self):
        return '<BucketItem %r>' % self.description


class Bucket(db.Model):
    """
    A bucket MUST belong to ONE user,
    MAY have one or more items and may have one or more hearts
    """
    __tablename__ = "bucket"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    create_date = db.Column(db.DateTime())
    complete_by_date = db.Column(db.DateTime())
    complete_by_text = db.Column(db.String(255))
    is_active = db.Column(db.Boolean, default=True)
    is_public = db.Column(db.Boolean, default=False)
    user_id = db.Column(db.Integer(), ForeignKey('user.id'))
    user = db.relationship("User", back_populates="user.buckets")
    items = db.relationship('BucketItem', back_populates='bucket_item.bucket')
    hearts = db.relationship('BucketHeart', back_populates='bucket_heart.bucket_id')

    def __repr__(self):
        return '<Bucket %r>' % self.name


class BucketHeart(db.Model):
    """
    Connecting table between user and buckets - A user may like one or more buckets
    """
    __tablename__ = "bucket_heart"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('user.id')),
    bucket_id = db.Column(db.Integer(), db.ForeignKey('bucket.id'))

    def __repr__(self):
        return '<User %r>' % self.username


class BucketItemHeart(db.Model):
    """
    Connecting table between user and items - A user may like one or more items
    """
    __tablename__ = "bucket_item_heart"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('user.id')),
    item_id = db.Column(db.Integer(), db.ForeignKey('bucket_item.id'))


class User(db.Model, UserMixin):
    """
    A user can have one or more buckets and one or more hearts
    (either bucket hearts or item hearts)
    """
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    # roles = db.relationship('Role', secondary=roles_users,
    #                         backref=db.backref('users', lazy='dynamic'))
    buckets = db.relationship("Bucket", back_populates="user")
    bucket_hearts = db.relationship('BucketHeart', back_populates='bucket_heart.user_id')
    item_hearts = db.relationship('BucketItemHeart', back_populates='bucket_heart.user_id')

    def __str__(self):
        return self.email

    def __repr__(self):
        return '<User %r>' % self.username

# Create admin
admin = flask_admin.Admin(
    app,
    'Example: Auth',
    base_template='my_master.html',
    template_mode='bootstrap3',
)

#
# # https://github.com/flask-admin/flask-admin/blob/master/examples/auth/app.py
# # Create customized model view class
# class MyModelView(ModelView):
#
#     def is_accessible(self):
#         if not current_user.is_active or not current_user.is_authenticated:
#             return False
#
#         if current_user.has_role('superuser'):
#             return True
#
#         return False
#
#     def _handle_view(self, name, **kwargs):
#         """
#         Override builtin _handle_view in order to redirect users when a view is not accessible.
#         """
#         if not self.is_accessible():
#             if current_user.is_authenticated:
#                 # permission denied
#                 abort(403)
#             else:
#                 # login
#                 return redirect(url_for('security.login', next=request.url))
#
#
# # # Add model views
# # admin.add_view(MyModelView(Role, db.session))
# # admin.add_view(MyModelView(User, db.session))
# #
# # # define a context processor for merging flask-admin's template context into the
# # # flask-security views.
# # @security.context_processor
# # def security_context_processor():
# #     return dict(
# #         admin_base_template=admin.base_template,
# #         admin_view=admin.index_view,
# #         h=admin_helpers,
# #         get_url=url_for
# #     )